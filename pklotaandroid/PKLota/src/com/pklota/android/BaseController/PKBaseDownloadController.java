package com.pklota.android.BaseController;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.ParseException;
import org.apache.http.StatusLine;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.pklota.android.Pojo.Log;
import com.pklota.android.Pojo.RequestTypes;
import com.pklota.android.Pojo.Response;
import com.pklota.android.listener.OnTaskCompleted;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.text.TextUtils;

/**
 * The JCPBaseController can be used to make asynchronous GET, POST, PUT and
 * DELETE HTTP requests in Android applications.Responses can be handled by
 * passing an anonymously.
 */
public class PKBaseDownloadController extends AsyncTask<String, Void, Response> {

	private static final String TECHNICAL_ISSUE = "technical issue";
	private static final String SOCKET_TIME_OUT = "We couldn't establish a connection to the server.Please try later";
	private static final String CAN_T_RESOLVE_HOST = "Unable to connect to JCPenney.Please check your network settings.";
	private Context mContext;
	private ProgressDialog mProgressDialog;
	private String mReqestUrl;
	private Map<String, String> mRequestParams;
	private OnTaskCompleted mController;
	private RequestTypes reqType;

	private static final int DEFAULT_SOCKET_TIMEOUT = 60 * 1000;
	private static int socketTimeout = DEFAULT_SOCKET_TIMEOUT;
	private static SharedPreferences mSharedpreferences;
	private int httppReqMethodType = GET_REQ;
	private boolean mIsEnabled = false;
	private boolean running;
	private JSONObject jsonParams;
	private DefaultHttpClient client;
	private static CookieStore cookieStore = null;
	// request types
	public static final int POST_REQ = 10;
	public static final int GET_REQ = 11;
	public static final int PUT_REQ = 12;
	public static final int DELETE_REQ = 13;

	public PKBaseDownloadController(Context context, OnTaskCompleted controller) {
		this.mContext = context;
		this.mController = controller;
		createSharedPrefrances(mContext);
	}

	public void disableProgressDialog(boolean isEnabled) {
		this.mIsEnabled = isEnabled;
	}

	private static void createSharedPrefrances(Context context) {
		mSharedpreferences = context.getSharedPreferences(
				"com.jcpenney.activities", Context.MODE_PRIVATE);
	}

	@Override
	protected void onPreExecute() {
		if (!((Activity) mContext).isFinishing()) {
			mProgressDialog = new ProgressDialog(mContext);
			mProgressDialog.setMessage("Please Wait...");
			mProgressDialog.setCancelable(false);
			if (!mProgressDialog.isShowing() && !mIsEnabled) {
				mProgressDialog.show();
			}
		}
		super.onPreExecute();
	}

	public void cancelPrevious() {
		running = true;
	}

	@Override
	protected Response doInBackground(String... params) {
		Response response = new Response();
		try {

			HttpResponse httpResponse = null;
			// Create local HTTP context
			if (Integer.parseInt(Build.VERSION.SDK) < Build.VERSION_CODES.FROYO) {
				System.setProperty("http.keepAlive", "false");
			}
			response.setReJcpRequestTypes(reqType);
			response.setStatusCode(-1);
			CookieHandler.setDefault(new CookieManager());
			BasicHttpParams httpParams = new BasicHttpParams();
			ConnManagerParams.setTimeout(httpParams, socketTimeout);
			HttpConnectionParams.setSoTimeout(httpParams, socketTimeout);
			HttpConnectionParams
					.setConnectionTimeout(httpParams, socketTimeout);
			HttpConnectionParams.setTcpNoDelay(httpParams, true);
			HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);
			// client = RequestController.getmHttpClient();
			client = new DefaultHttpClient();
			if (httppReqMethodType == GET_REQ) {
				httpResponse = client.execute(getHttpGet(mReqestUrl));
			} else if (httppReqMethodType == POST_REQ) {
				httpResponse = client.execute(getHttpPost(mReqestUrl,
						mRequestParams));
			} else if (httppReqMethodType == PUT_REQ) {
				httpResponse = client.execute(getHttpPut(mReqestUrl,
						mRequestParams));
			} else if (httppReqMethodType == DELETE_REQ) {
				httpResponse = client.execute(getHttpDelete(mReqestUrl));
			}

			if (this.getReqType() == RequestTypes.TEXT_LIST_REQ) {
				if (running) {
					response.setReJcpRequestTypes(reqType);
					response.setStatusCode(-2);
					response.setResponseBoday("");
					response.setErrorMsg("");
					return response;
				}
			} /*else if (this.getReqType() == RequestTypes.SHIPPING_STATUS_REQ) {
				if (running) {
					response.setReJcpRequestTypes(reqType);
					response.setStatusCode(-2);
					response.setResponseBoday("");
					response.setErrorMsg("");
					return response;
				}
			} else if (this.getReqType() == RequestTypes.SAVEITEM_POST) {
				if (running) {
					response.setReJcpRequestTypes(reqType);
					response.setStatusCode(-2);
					response.setResponseBoday("");
					response.setErrorMsg("");
					return response;
				}
			} else if (this.getReqType() == RequestTypes.PRODUCTDETAILS_REQ) {
				if (running) {
					response.setReJcpRequestTypes(reqType);
					response.setStatusCode(-2);
					response.setResponseBoday("");
					response.setErrorMsg("");
					return response;
				}
			} else if (this.getReqType() == RequestTypes.RECOMMENDATION_REQ) {
				if (running) {
					response.setReJcpRequestTypes(reqType);
					response.setStatusCode(-2);
					response.setResponseBoday("");
					response.setErrorMsg("");
					return response;
				}
			} else if (this.getReqType() == RequestTypes.REVIEW_REQ) {
				if (running) {
					response.setReJcpRequestTypes(reqType);
					response.setStatusCode(-2);
					response.setResponseBoday("");
					response.setErrorMsg("");
					return response;
				}
			}*/
			if (httpResponse != null) {

				StatusLine status = httpResponse.getStatusLine();
				String responseBody = null;
				HttpEntity entity = null;
				HttpEntity temp = httpResponse.getEntity();
				if (temp != null) {
					entity = new BufferedHttpEntity(temp);
					responseBody = EntityUtils.toString(entity, "UTF-8");
				}
				response.setReJcpRequestTypes(reqType);
				response.setStatusCode(status.getStatusCode());
				response.setResponseBoday(responseBody);
				response.setOptionalMsg(status.getReasonPhrase());
			} else {

				response.setResponseBoday(CAN_T_RESOLVE_HOST);// "Error in network";
				response.setOptionalMsg("");
			}
		} catch (UnknownHostException e) {
			response.setResponseBoday(CAN_T_RESOLVE_HOST);
			response.setOptionalMsg(e.getMessage());
		} catch (SocketException e) {
			response.setResponseBoday(CAN_T_RESOLVE_HOST);
			response.setOptionalMsg(e.getMessage());
		} catch (SocketTimeoutException e) {

			response.setResponseBoday(SOCKET_TIME_OUT);
			response.setOptionalMsg(e.getMessage());
		} catch (IOException e) {
			response.setResponseBoday(CAN_T_RESOLVE_HOST);
			response.setOptionalMsg(e.getMessage());
		} finally {
			// safeClose(client);
		}
		return response;
	}

	public static void safeClose(HttpClient client) {
		if (client != null && client.getConnectionManager() != null) {
			client.getConnectionManager().shutdown();
		}
	}

	@Override
	protected void onPostExecute(Response response) {
		if (mProgressDialog != null && mProgressDialog.isShowing()) {
			mProgressDialog.dismiss();
		}
		if (mController != null) {
			mController.onTaskCompleted(response);
		}

		super.onPostExecute(response);
	}

	/**
	 * Perform a HTTP GET request, without any parameters.
	 *
	 * @param reqUrl
	 *            the URL to send the request to.
	 */
	private HttpGet getHttpGet(String reqUrl) {
		HttpGet httpGet = new HttpGet(reqUrl);
		httpGet.setHeader("Content-type", "application/json");
		return httpGet;
	}

	/**
	 * Perform a HTTP POST request, without any parameters.
	 *
	 * @param url
	 *            the URL to send the request to.
	 * @param reqParams
	 *            parameters to send the request to.
	 */
	private HttpPost getHttpPost(String reqUrl, Map<String, String> reqParams) {
		HttpPost httpPost = null;
		try {
			httpPost = new HttpPost(mReqestUrl);
			httpPost.setHeader("Content-type", "application/json");
			String httpReqParams = null;
			if (mRequestParams != null) {
				httpReqParams = new JSONObject(mRequestParams).toString();
			}
			if (jsonParams != null) {
				httpReqParams = jsonParams.toString();
			}
			if (httpReqParams != null) {
				StringEntity formEntity = new StringEntity(httpReqParams,
						"utf-8");
				httpPost.setEntity(formEntity);
			}

			return httpPost;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Perform a HTTP PUT request, without any parameters.
	 *
	 * @param reqUrl
	 *            the URL to send the request to.
	 * @param reqParams
	 *            parameters to send the request to.
	 */
	private HttpPut getHttpPut(String reqUrl, Map<String, String> reqParams) {
		HttpPut httpPut = null;
		try {
			httpPut = new HttpPut(mReqestUrl);
			httpPut.setHeader("Content-type", "application/json");
			StringEntity formEntity = new StringEntity(new JSONObject(
					mRequestParams).toString(), "utf-8");
			httpPut.setEntity(formEntity);

			return httpPut;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}

	private HttpDelete getHttpDelete(String reqUrl) {
		HttpDelete httpDelete = new HttpDelete(reqUrl);

		return httpDelete;

	}

	public RequestTypes getReqType() {
		return reqType;
	}

	public void setReqType(RequestTypes reqType) {
		this.reqType = reqType;
	}

	public int getHttpReqMethodType() {
		return httppReqMethodType;
	}

	public void setHttpReqMethodType(int httppReqMethodType) {
		this.httppReqMethodType = httppReqMethodType;
	}

	public String getUrl() {
		return mReqestUrl;
	}

	public void setUrl(String url) {
		this.mReqestUrl = url;
	}

	public Map<String, String> getParameters() {
		return mRequestParams;
	}

	public void setParameters(Map<String, String> parameters) {
		this.mRequestParams = parameters;
	}

	public void setJSONParameters(JSONObject pProductObj) {
		this.jsonParams = pProductObj;

	}
}
