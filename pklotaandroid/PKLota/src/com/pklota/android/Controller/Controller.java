package com.pklota.android.Controller;

import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.text.Html;
import android.util.Log;

import com.google.gson.Gson;
import com.pklota.android.BaseController.PKBaseController;
import com.pklota.android.BaseController.PKBaseDownloadController;
import com.pklota.android.Pojo.AudioPojo;
import com.pklota.android.Pojo.ImagePojo;
import com.pklota.android.Pojo.RequestTypes;
import com.pklota.android.Pojo.Response;
import com.pklota.android.Pojo.SignleImage;
import com.pklota.android.Pojo.TextPojo;
import com.pklota.android.Pojo.VideoPojo;
import com.pklota.android.Util.Util;
import com.pklota.android.listener.OnTaskCompleted;
import com.pklota.android.listener.PK_AudioListner;
import com.pklota.android.listener.PK_ImageListner;
import com.pklota.android.listener.PK_SingleImageListener;
import com.pklota.android.listener.PK_TextListner;
import com.pklota.android.listener.PK_VideoListner;

public class Controller implements OnTaskCompleted {

	private static Controller mJCpFindInStoreController;
	private Context mContext;
	private String mFragmentType;
	private PK_TextListner mTextListener;
	private PK_AudioListner mAudioListener;
	private PK_VideoListner mVideoListener;
	private PK_ImageListner mImageListener;
	private PK_SingleImageListener mSignleImagelistener;

	public static Controller getInstance(Context context) {
		// if(mJCpFindInStoreController == null){
		mJCpFindInStoreController = new Controller(context);
		// }
		return mJCpFindInStoreController;

	}

	private Controller(Context context) {
		this.mContext = context;
	}

	public void setListener(PK_TextListner listener) {
		this.mTextListener = listener;
	}

	public void setListener(PK_AudioListner listener) {
		this.mAudioListener = listener;
	}

	public void setListener(PK_VideoListner listener) {
		this.mVideoListener = listener;
	}

	public void setListener(PK_ImageListner listener) {
		this.mImageListener = listener;
	}

	public void setListenerForImage(PK_SingleImageListener listener) {
		this.mSignleImagelistener = listener;
	}

	@Override
	public void onTaskCompleted(Response response) {
		RequestTypes reqType = response.getReJcpRequestTypes();
		int statusCode = response.getStatusCode();
		String data = response.getResponseBoday();
		if (reqType == RequestTypes.TEXT_LIST_REQ) {
			if (statusCode == HttpStatus.SC_CREATED
					|| statusCode == HttpStatus.SC_OK) {
				mTextListener.getTextDataResponse(parseTextData(data), reqType);
			} else if (statusCode == -1) {
				mTextListener.onFailure(data, reqType);
			} else {
				mTextListener.onFailure(getErrorMessage(data), reqType);

			}
		} else if (reqType == RequestTypes.IMAGE_THUMBNAIL_LIST_REQ) {
			if (statusCode == HttpStatus.SC_CREATED
					|| statusCode == HttpStatus.SC_OK) {
				mImageListener.getImageDataResponse(parseImageData(data),
						reqType);
			} else if (statusCode == -1) {
				mImageListener.onFailure(data, reqType);
			} else {
				mImageListener.onFailure(getErrorMessage(data), reqType);

			}
		} else if (reqType == RequestTypes.VIDEO_THUMBNAIL_LIST_REQ) {
			if (statusCode == HttpStatus.SC_CREATED
					|| statusCode == HttpStatus.SC_OK) {
				mVideoListener.getVideoDataResponse(parseVideoData(data),
						reqType);
			} else if (statusCode == -1) {
				mVideoListener.onFailure(data, reqType);
			} else {
				mVideoListener.onFailure(getErrorMessage(data), reqType);

			}
		} else if (reqType == RequestTypes.AUDIO_LIST_REQ) {
			if (statusCode == HttpStatus.SC_CREATED
					|| statusCode == HttpStatus.SC_OK) {
				mAudioListener.getAudioDataResponse(parseAudioData(data),
						reqType);
			} else if (statusCode == -1) {
				mAudioListener.onFailure(data, reqType);
			} else {
				mAudioListener.onFailure(getErrorMessage(data), reqType);

			}
		} else if (reqType == RequestTypes.SINGLE_VIDEO_REQ) {
			if (statusCode == HttpStatus.SC_CREATED
					|| statusCode == HttpStatus.SC_OK) {
				mVideoListener.getSingleVideoDataResponse(
						parseSingleVideoData(data), reqType);
			} else if (statusCode == -1) {
				mVideoListener.onFailure(data, reqType);
			} else {
				mVideoListener.onFailure(getErrorMessage(data), reqType);

			}
		} else if (reqType == RequestTypes.SINGLE_IMAGE_REQ) {
			if (statusCode == HttpStatus.SC_CREATED
					|| statusCode == HttpStatus.SC_OK) {
				if(mSignleImagelistener!=null){
				mSignleImagelistener.getSingleImageDataResponse(
						parseSingleImageData(data), reqType);
				}else{
					
				}
			} else if (statusCode == -1) {
				mImageListener.onFailure(data, reqType);
			} else {
				mImageListener.onFailure(getErrorMessage(data), reqType);

			}
		}

	}

	private VideoPojo parseSingleVideoData(String data) {
		Gson gson = new Gson();
		VideoPojo pojoEntityContainer = gson.fromJson(data, VideoPojo.class);

		return pojoEntityContainer;
	}

	private SignleImage parseSingleImageData(String data) {
		Gson gson = new Gson();
		SignleImage pojoEntityContainer = gson
				.fromJson(data, SignleImage.class);

		return pojoEntityContainer;
	}

	private AudioPojo parseAudioData(String data) {
		Gson gson = new Gson();
		AudioPojo pojoEntityContainer = gson.fromJson(data, AudioPojo.class);

		return pojoEntityContainer;
	}

	private VideoPojo parseVideoData(String data) {
		Gson gson = new Gson();
		VideoPojo pojoEntityContainer = gson.fromJson(data, VideoPojo.class);

		return pojoEntityContainer;
	}

	private ImagePojo parseImageData(String data) {
		// TODO Auto-generated method stub
		Gson gson = new Gson();
		ImagePojo pojoEntityContainer = gson.fromJson(data, ImagePojo.class);

		return pojoEntityContainer;
	}

	private TextPojo parseTextData(String data) {
		// TODO Auto-generated method stub
		Gson gson = new Gson();
		TextPojo pojoEntityContainer = gson.fromJson(data, TextPojo.class);

		return pojoEntityContainer;
	}

	private String getErrorMessage(String data) {
		String errorMsg = null;
		try {
			JSONObject jsonObject = new JSONObject(data);
			if (jsonObject != null) {
				if (jsonObject.has("errorMessage")) {
					errorMsg = jsonObject.getString("errorMessage");
				}
			}
		} catch (JSONException e) {
			errorMsg = Html.fromHtml(data).toString();
		}
		return errorMsg;
	}

	public void getTextDataOperation() {
		// TODO Auto-generated method stub
		String url = Util.getTextDetailsUrl();
		PKBaseController jcpBaseController = new PKBaseController(mContext,
				this);
		jcpBaseController.setUrl(url);
		jcpBaseController.setReqType(RequestTypes.TEXT_LIST_REQ);
		jcpBaseController.setHttpReqMethodType(PKBaseController.GET_REQ);
		jcpBaseController.execute();
	}

	public void getImageThumbnailOperation() {
		// TODO Auto-generated method stub
		String url = Util.getImageDetailsUrl();
		PKBaseController jcpBaseController = new PKBaseController(mContext,
				this);
		jcpBaseController.setUrl(url);
		jcpBaseController.setReqType(RequestTypes.IMAGE_THUMBNAIL_LIST_REQ);
		jcpBaseController.setHttpReqMethodType(PKBaseController.GET_REQ);
		jcpBaseController.execute();
	}
	
	public void getVideoThumbnailOperation() {
		// TODO Auto-generated method stub
		String url = Util.getVideoDetailsUrl();
		PKBaseController jcpBaseController = new PKBaseController(mContext,
				this);
		jcpBaseController.setUrl(url);
		jcpBaseController.setReqType(RequestTypes.VIDEO_THUMBNAIL_LIST_REQ);
		jcpBaseController.setHttpReqMethodType(PKBaseController.GET_REQ);
		jcpBaseController.execute();
	}

	public void getVideoOperation() {
		// TODO Auto-generated method stub
		String url = Util.getVideoDetailsUrl();
		PKBaseController jcpBaseController = new PKBaseController(mContext,
				this);
		jcpBaseController.setUrl(url);
		jcpBaseController.setReqType(RequestTypes.VIDEO_THUMBNAIL_LIST_REQ);
		jcpBaseController.setHttpReqMethodType(PKBaseController.GET_REQ);
		jcpBaseController.execute();
	}

	public void getAudioOperation() {
		// TODO Auto-generated method stub
		String url = Util.getAudioDetailsUrl();
		PKBaseController jcpBaseController = new PKBaseController(mContext,
				this);
		jcpBaseController.setUrl(url);
		jcpBaseController.setReqType(RequestTypes.AUDIO_LIST_REQ);
		jcpBaseController.setHttpReqMethodType(PKBaseController.GET_REQ);
		jcpBaseController.execute();
	}

	public void getIndividualImageOperation(String id) {
		// TODO Auto-generated method stub
		String url = Util.getIndividualImageUrl(id);
		PKBaseDownloadController jcpBaseController = new PKBaseDownloadController(
				mContext, this);
		// jcpBaseController.setParameters(id);
		jcpBaseController.setUrl(url);
		jcpBaseController.setReqType(RequestTypes.SINGLE_IMAGE_REQ);
		jcpBaseController.setHttpReqMethodType(PKBaseController.GET_REQ);
		jcpBaseController.execute();
	}

	public void getIndividualVideoOperation(String id) {
		// TODO Auto-generated method stub
		String url = Util.getIndividualVideoUrl();
		PKBaseController jcpBaseController = new PKBaseController(mContext,
				this);
		jcpBaseController.setUrl(url);
		// jcpBaseController.setParameters(id);
		jcpBaseController.setReqType(RequestTypes.SINGLE_VIDEO_REQ);
		jcpBaseController.setHttpReqMethodType(PKBaseController.POST_REQ);
		jcpBaseController.execute();
	}

}
