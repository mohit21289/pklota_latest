package com.pklota.android.Fragment;

import com.pklota.android.R;
import com.pklota.android.Controller.Controller;
import com.pklota.android.Pojo.AudioPojo;
import com.pklota.android.Pojo.RequestTypes;
import com.pklota.android.adopter.CustomAudioAdapter;
import com.pklota.android.adopter.CustomvideoAdapter;
import com.pklota.android.listener.PK_AudioListner;
import com.pklota.android.listener.PK_VideoListner;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class PK_AudioFragment extends Fragment implements PK_AudioListner {
	ListView list_view;
	private Controller mController;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.activity_main, container,
				false);
		list_view = (ListView) rootView.findViewById(R.id.list_view);
		mController = Controller.getInstance(getActivity());
		mController.setListener(this);

		mController.getAudioOperation();

		return rootView;
	}

	@Override
	public void getAudioDataResponse(AudioPojo parseAudioData,
			RequestTypes reqType) {
		// TODO Auto-generated method stub
		list_view.setAdapter(new CustomAudioAdapter(getActivity(),parseAudioData.getAudioPostTO()));
	}

	@Override
	public void onFailure(String data, RequestTypes reqType) {
		// TODO Auto-generated method stub

	}

	@Override
	public void getAudioDataResponse(String result) {
		// TODO Auto-generated method stub
		
	}
}
