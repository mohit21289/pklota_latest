package com.pklota.android.Fragment;

import java.util.ArrayList;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.pklota.android.R;
import com.pklota.android.Controller.Controller;
import com.pklota.android.Pojo.ImagePojo;
import com.pklota.android.Pojo.ImageThumbPostTO;
import com.pklota.android.Pojo.RequestTypes;
import com.pklota.android.Util.AbsListViewBaseFragment;
import com.pklota.android.adopter.CustomImageAdopter;
import com.pklota.android.grid.StaggeredGridView;
import com.pklota.android.listener.PK_ImageListner;
import android.content.Context;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class PK_ImageFragment extends AbsListViewBaseFragment implements
		PK_ImageListner {
	private static final SparseArray<Double> sPositionHeightRatios = new SparseArray<Double>();
	boolean bol;
	private Controller mController;
	DisplayImageOptions options;
	StaggeredGridView list_view;
	ArrayList<ImageThumbPostTO> ImagePojo = new ArrayList<ImageThumbPostTO>();
	ImageThumbPostTO ImageThumbPostTO;
	Context ctx;
	ArrayList<ImageThumbPostTO> listarray;
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.activity_sgv, container,
				false);
		list_view = (StaggeredGridView) rootView.findViewById(R.id.list_view);
		mController = Controller.getInstance(getActivity());
		mController.setListener(this);
		mController.getImageThumbnailOperation();
		return rootView;
	}


	@Override
	public void getImageDataResponse(ImagePojo parseImageData,
			RequestTypes reqType) {
		// TODO Auto-generated method stub
		list_view.setAdapter(new CustomImageAdopter(getActivity(),
				parseImageData.getImageThumbPostTO(), false));
	}

	@Override
	public void onFailure(String data, RequestTypes reqType) {
		// TODO Auto-generated method stub

	}

	

}
