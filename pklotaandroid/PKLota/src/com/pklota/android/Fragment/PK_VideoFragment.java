package com.pklota.android.Fragment;

import com.pklota.android.R;
import com.pklota.android.Controller.Controller;
import com.pklota.android.Pojo.RequestTypes;
import com.pklota.android.Pojo.TextPojo;
import com.pklota.android.Pojo.VideoPojo;
import com.pklota.android.Util.GetCacheDirExample;
import com.pklota.android.adopter.CustomvideoAdapter;
import com.pklota.android.adopter.TextCustomAdopter;
import com.pklota.android.listener.PK_VideoListner;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class PK_VideoFragment extends Fragment implements PK_VideoListner {
	ListView list_view;
	private Controller mController;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.activity_main, container,
				false);
		list_view = (ListView) rootView.findViewById(R.id.list_view);
		mController = Controller.getInstance(getActivity());
		mController.setListener(this);

		mController.getVideoOperation();

		return rootView;
	}

	@Override
	public void onFailure(String data, RequestTypes reqType) {
		// TODO Auto-generated method stub

	}

	@Override
	public void getVideoDataResponse(VideoPojo parseVideoData, RequestTypes reqType) {
		// TODO Auto-generated method stub
		list_view.setAdapter(new CustomvideoAdapter(getActivity(),parseVideoData.getVideoThumbPostTO()));
	}

	@Override
	public void getSingleVideoDataResponse(VideoPojo parseSingleVideoData,
			RequestTypes reqType) {
		// TODO Auto-generated method stub

	}
}
