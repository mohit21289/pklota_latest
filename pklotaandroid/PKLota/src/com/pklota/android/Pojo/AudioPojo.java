package com.pklota.android.Pojo;

import java.util.ArrayList;

public class AudioPojo {

	private ArrayList<AudioPostTO> audioPostTO = new ArrayList<AudioPostTO>();

	public ArrayList<AudioPostTO> getAudioPostTO() {
		return audioPostTO;
	}

	public void setAudioPostTO(ArrayList<AudioPostTO> audioPostTO) {
		this.audioPostTO = audioPostTO;
	}

}