package com.pklota.android.Pojo;

import java.util.ArrayList;

public class ImagePojo {

	
		private ArrayList<ImageThumbPostTO> imageThumbPostTO = new ArrayList<ImageThumbPostTO>();

		/**
		 * 
		 * @return The imageThumbPostTO
		 */
		public ArrayList<ImageThumbPostTO> getImageThumbPostTO() {
			return imageThumbPostTO;
		}

		/**
		 * 
		 * @param imageThumbPostTO
		 *            The imageThumbPostTO
		 */
		public void setImageThumbPostTO(
				ArrayList<ImageThumbPostTO> imageThumbPostTO) {
			this.imageThumbPostTO = imageThumbPostTO;
		}
	
}