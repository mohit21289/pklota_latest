package com.pklota.android.Pojo;



public class Log {
	public static final boolean DEBUG = true;

	public static void e(String tag, String msg) {
		if (DEBUG) {
			Log.e(tag, msg);
		}
	}

	public static void e(String tag, String msg, Exception e) {
		if (DEBUG) {
			Log.e(tag, msg, e);
		}
	}

	public static void w(String tag, String msg) {
		if (DEBUG) {
			Log.w(tag, msg);
		}
	}

	public static void w(String tag, String msg, Exception e) {
		if (DEBUG) {
			Log.w(tag, msg, e);
		}
	}

	public static void i(String tag, String msg) {
		if (DEBUG) {
			Log.i(tag, msg);
		}
	}

	public static void i(String tag, String msg, Exception e) {
		if (DEBUG) {
			Log.i(tag, msg, e);
		}
	}

	public static void d(String tag, String msg) {
		if (DEBUG) {
			Log.d(tag, msg);
		}
	}

	public static void d(String tag, String msg, Exception e) {
		if (DEBUG) {
			Log.d(tag, msg, e);
		}
	}

}
