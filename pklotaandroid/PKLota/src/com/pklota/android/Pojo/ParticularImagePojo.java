package com.pklota.android.Pojo;
                 
public class ParticularImagePojo
{
    private String ipid;

    private String imageBytes;

    public String getIpid ()
    {
        return ipid;
    }

    public void setIpid (String ipid)
    {
        this.ipid = ipid;
    }

    public String getImageBytes ()
    {
        return imageBytes;
    }

    public void setImageBytes (String imageBytes)
    {
        this.imageBytes = imageBytes;
    }
}
	