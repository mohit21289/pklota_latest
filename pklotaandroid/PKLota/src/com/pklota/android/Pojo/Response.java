package com.pklota.android.Pojo;


public class Response {

	private int statusCode;
	private String responseBoday;
	private String errorMsg;
	private String optionalMsg;
	private RequestTypes reJcpRequestTypes;
	 
	public RequestTypes getReJcpRequestTypes() {
		return reJcpRequestTypes;
	}
	public void setReJcpRequestTypes(RequestTypes reJcpRequestTypes) {
		this.reJcpRequestTypes = reJcpRequestTypes;
	}
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public String getResponseBoday() {
		return responseBoday;
	}
	public void setResponseBoday(String responseBoday) {
		this.responseBoday = responseBoday;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public String getOptionalMsg() {
		return optionalMsg;
	}
	public void setOptionalMsg(String optionalMsg) {
		this.optionalMsg = optionalMsg;
	}
}
