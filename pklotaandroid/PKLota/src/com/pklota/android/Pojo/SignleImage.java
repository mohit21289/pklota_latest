package com.pklota.android.Pojo;


public class SignleImage {


	private String imageUrl;

	private String ipid;

	/**
	 * 
	 * @return The imageUrl
	 */
	public String getImageUrl() {
		return imageUrl;
	}

	/**
	 * 
	 * @param imageUrl
	 *            The imageUrl
	 */
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	/**
	 * 
	 * @return The ipid
	 */
	public String getIpid() {
		return ipid;
	}

	/**
	 * 
	 * @param ipid
	 *            The ipid
	 */
	public void setIpid(String ipid) {
		this.ipid = ipid;
	}

}