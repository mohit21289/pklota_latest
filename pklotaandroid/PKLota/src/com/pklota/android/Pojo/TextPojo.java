package com.pklota.android.Pojo;

import java.util.ArrayList;

public class TextPojo {

	private ArrayList<TextPostTO> textPostTO = new ArrayList<TextPostTO>();

	public ArrayList<TextPostTO> getPostTO() {
		return textPostTO;
	}

	public void setPostTO(ArrayList<TextPostTO> postTO) {
		this.textPostTO = postTO;
	}

}
