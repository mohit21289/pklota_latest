package com.pklota.android.Pojo;
public class TextPostTO
{
    private String allowed;

    private String postDate;

    private String pid;

    private String postContent;

    public String getAllowed ()
    {
        return allowed;
    }

    public void setAllowed (String allowed)
    {
        this.allowed = allowed;
    }

    public String getPostDate ()
    {
        return postDate;
    }

    public void setPostDate (String postDate)
    {
        this.postDate = postDate;
    }

    public String getPid ()
    {
        return pid;
    }

    public void setPid (String pid)
    {
        this.pid = pid;
    }

    public String getPostContent ()
    {
        return postContent;
    }

    public void setPostContent (String postContent)
    {
        this.postContent = postContent;
    }
}