package com.pklota.android.Pojo;

import java.util.ArrayList;


public class VideoPojo {

	
	private ArrayList<VideoThumbPostTO> videoThumbPostTO = new ArrayList<VideoThumbPostTO>();

	/**
	 * 
	 * @return The videoThumbPostTO
	 */
	public ArrayList<VideoThumbPostTO> getVideoThumbPostTO() {
		return videoThumbPostTO;
	}

	/**
	 * 
	 * @param videoThumbPostTO
	 *            The videoThumbPostTO
	 */
	public void setVideoThumbPostTO(ArrayList<VideoThumbPostTO> videoThumbPostTO) {
		this.videoThumbPostTO = videoThumbPostTO;
	}

}