package com.pklota.android.Pojo;

public class VideoThumbPostTO {

	private String vpid;
	private String videoThumbUrl;

	public String getVpid() {
		return vpid;
	}

	public void setVpid(String vpid) {
		this.vpid = vpid;
	}
	public String getVideoThumbUrl() {
		return videoThumbUrl;
	}
	public void setVideoThumbUrl(String videoThumbUrl) {
		this.videoThumbUrl = videoThumbUrl;
	}
}
