package com.pklota.android.Util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class Util {

	private static String baseUrl = "https://m-dt-test2.jcpenney.com/v2/";

	/** The time it takes for our client to timeout */
	public static final int HTTP_TIMEOUT = 30 * 1000; // milliseconds

	/** Single instance of our HttpClient */
	private static HttpClient mHttpClient;
	private static String cookies;
	static SharedPreferences mSharedpreferences;
	/**
	 * Get our single instance of our HttpClient object
	 * 
	 * @return an HttpClient object with connection parameters set
	 */

	public static String sessionId = null, DPSecureCookie = null;
	public static int ResponseCode = 0;

	private static HttpResponse response;

	private static Activity mActivity;

	public static Activity getmActivity() {
		return mActivity;
	}

	public static void setmActivity(Activity mActivity) {
		Util.mActivity = mActivity;
	}

	public static enum FilterType {
		BED_SIZE, PRICE_RANGE, AVERAGE_RATING, COLOR, COMMON;
	}

	public static enum Type {
		VIEW_ALL, VIEW_LESS, COMM;
	}

	public static String getTextDetailsUrl() {
		String url;
		url = "http://pklota-pklota.rhcloud.com/services/TextPostServiceImpl/getAllTextPost";
		/*
		 * url = baseUrl + "stores?latitude=" + lat_current + "&longitude=" +
		 * lng_current+"&dpAkamaiOverride=1";
		 */
		// TODO Auto-generated method stub
		return url;
	}

	public static String getImageDetailsUrl() {
		String url;
		url = "http://pklota-pklota.rhcloud.com/services/ImagePostServiceImpl/getAllImageThumbPost";
		/*
		 * url = baseUrl + "stores?latitude=" + lat_current + "&longitude=" +
		 * lng_current+"&dpAkamaiOverride=1";
		 */
		// TODO Auto-generated method stub
		return url;
	}

	public static String getImageDetailsParticularUrl(String str) {
		String url;
		url = "http://pklota-pklota.rhcloud.com/services/ImagePostServiceImpl/getImagePostById/"
				+ str;
		return url;
	}

	public static String convertToCommaDelimited(String[] list) {
		StringBuffer ret = new StringBuffer("");
		for (int i = 0; list != null && i < list.length; i++) {
			ret.append(list[i]);
			if (i < list.length - 1) {
				ret.append(',');
			}
		}
		return ret.toString();
	}

	public static String convertStreamToString(InputStream is) {

		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

	private static void createSharedPrefrances(Context context) {
		mSharedpreferences = context.getSharedPreferences(
				"com.jcpenny.activities", Context.MODE_PRIVATE);
	}

	private static HttpClient getHttpClient() {
		if (mHttpClient == null) {
			mHttpClient = new DefaultHttpClient();
			final HttpParams params = mHttpClient.getParams();
			HttpConnectionParams.setConnectionTimeout(params, HTTP_TIMEOUT);
			HttpConnectionParams.setSoTimeout(params, HTTP_TIMEOUT);
			ConnManagerParams.setTimeout(params, HTTP_TIMEOUT);
		}
		return mHttpClient;
	}

	public static String executeHttpPost(String url,
			Map<String, String> postParameters, Context context) {
		BufferedReader in = null;
		String result = "";
		createSharedPrefrances(context);
		try {
			HttpClient client = getHttpClient();
			HttpPost request = new HttpPost(url);
			request.setHeader("Content-type", "application/json");
			Log.d("post_data",
					"post data" + new JSONObject(postParameters).toString());

			Log.d("avinash", "get cookie=" + getCookies());
			// request.setHeader("Cookie", getCookies());
			StringEntity formEntity = new StringEntity(new JSONObject(
					postParameters).toString(), "utf-8");
			request.setEntity(formEntity);

			HttpResponse response = client.execute(request);
			ResponseCode = response.getStatusLine().getStatusCode();
			// storing cookie
			setCookies(response.getFirstHeader("Set-Cookie") == null ? ""
					: response.getFirstHeader("Set-Cookie").toString());
			Log.d("avinash", "cookie="
					+ response.getFirstHeader("Set-Cookie").toString());
			Log.d("post_data", "response code=" + ResponseCode);
			in = new BufferedReader(new InputStreamReader(response.getEntity()
					.getContent()));

			StringBuffer sb = new StringBuffer("");
			String line = "";
			String NL = System.getProperty("line.separator");
			while ((line = in.readLine()) != null) {
				sb.append(line + NL);
			}
			in.close();

			result = sb.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;

	}

	public static String executeHttpGet(String url, Context context)
			throws Exception {
		BufferedReader in = null;
		String result = "";
		createSharedPrefrances(context);
		try {

			HttpClient client = getHttpClient();
			HttpGet request = new HttpGet(url);

			HttpResponse response = client.execute(request);
			ResponseCode = response.getStatusLine().getStatusCode();

			in = new BufferedReader(new InputStreamReader(response.getEntity()
					.getContent()));

			StringBuffer sb = new StringBuffer("");
			String line = "";

			while ((line = in.readLine()) != null) {
				sb.append(line);
			}
			in.close();

			result = sb.toString();
			return result;
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static String executeHttpPut(String url,
			Map<String, String> postParameters, Context context)
			throws Exception {

		BufferedReader in = null;
		String result = "";
		createSharedPrefrances(context);
		try {
			HttpClient client = getHttpClient();
			HttpPut request = new HttpPut(url);
			// request.setHeader("Cookie", getCookies());
			request.setHeader("Content-type", "application/json");
			// Log.d("avinash", "get cookie="
			// + getCookies());
			StringEntity formEntity = new StringEntity(new JSONObject(
					postParameters).toString(), "utf-8");
			Log.d("srikant: executeHttpPut", "formEntity: " + formEntity);
			request.setEntity(formEntity);

			HttpResponse response = client.execute(request);
			ResponseCode = response.getStatusLine().getStatusCode();
			// storing cookie
			// storing cookie
			Log.d("avinash", "cookie="
					+ response.getFirstHeader("Set-Cookie").toString());
			setCookies(response.getFirstHeader("Set-Cookie") == null ? ""
					: response.getFirstHeader("Set-Cookie").toString());

			in = new BufferedReader(new InputStreamReader(response.getEntity()
					.getContent()));

			StringBuffer sb = new StringBuffer("");
			String line = "";
			String NL = System.getProperty("line.separator");
			while ((line = in.readLine()) != null) {
				sb.append(line + NL);
			}
			in.close();

			result = sb.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}

		Log.d("srikat:executeHttpPut", result);

		return result;

	}

	public static String executeHttpDelete(String url, Context context)
			throws Exception {
		BufferedReader in = null;
		String result = "";
		createSharedPrefrances(context);
		try {

			HttpClient client = getHttpClient();
			HttpDelete request = new HttpDelete(url);
			// request.setHeader("Cookie", getCookies());
			// Log.d("avinash", "get cookie="
			// + getCookies());
			HttpResponse response = client.execute(request);
			ResponseCode = response.getStatusLine().getStatusCode();
			// storing cookie
			// setCookies(response.getFirstHeader("Set-Cookie") == null ? "" :
			// response.getFirstHeader("Set-Cookie").toString());
			// Log.d("avinash", "cookie="
			// + response.getFirstHeader("Set-Cookie").toString());
			in = new BufferedReader(new InputStreamReader(response.getEntity()
					.getContent()));

			StringBuffer sb = new StringBuffer("");
			String line = "";
			String NL = System.getProperty("line.separator");
			while ((line = in.readLine()) != null) {
				sb.append(line + NL);
			}
			in.close();

			result = sb.toString();
			return result;
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static String getCookies() {
		return mSharedpreferences.getString("cookies", "");
	}

	public static void setCookies(String cookies1) {
		SharedPreferences.Editor editor = mSharedpreferences.edit();
		editor.putString("cookies", cookies1);
		editor.commit();
		cookies = cookies1;
	}

	public static HttpResponse ProductExecuteHttpGet(String url, Context context)
			throws Exception {
		BufferedReader in = null;
		String result = "";
		createSharedPrefrances(context);
		try {
			HttpClient client = getHttpClient();
			HttpGet request = new HttpGet(url);
			// request.setHeader("Cookie", getCookies());
			Log.d("avinash", "get cookie=" + getCookies());
			response = client.execute(request);
			/*
			 * ResponseCode = response.getStatusLine().getStatusCode();
			 * //storing cookie setCookies(response.getFirstHeader("Set-Cookie")
			 * == null ? "" : response.getFirstHeader("Set-Cookie").toString());
			 * Log.d("avinash", "cookie=" +
			 * response.getFirstHeader("Set-Cookie").toString());
			 * Log.d("get_data", "response code=" + ResponseCode); in = new
			 * BufferedReader(new InputStreamReader(response.getEntity()
			 * .getContent()));
			 * 
			 * StringBuffer sb = new StringBuffer(""); String line = "";
			 * 
			 * while ((line = in.readLine()) != null) { sb.append(line); }
			 * in.close();
			 * 
			 * result = sb.toString();
			 */

		} catch (Exception e) {
			// TODO: handle exception
		}
		return response;

	}

	public static String getVideoDetailsUrl() {
		String url;
		url = "http://pklota-pklota.rhcloud.com/services/VideoPostServiceImpl/getAllVideoThumbPost";
		/*
		 * url = baseUrl + "stores?latitude=" + lat_current + "&longitude=" +
		 * lng_current+"&dpAkamaiOverride=1";
		 */
		// TODO Auto-generated method stub
		return url;
	}

	public static String getAudioDetailsUrl() {
		// TODO Auto-generated method stub
		String url;
		url = "http://pklota-pklota.rhcloud.com/services/AudioPostServiceImpl/getAllAudioPost";
		/*
		 * url = baseUrl + "stores?latitude=" + lat_current + "&longitude=" +
		 * lng_current+"&dpAkamaiOverride=1";
		 */
		// TODO Auto-generated method stub
		return url;
	}

	public static String getIndividualVideoUrl() {
		// TODO Auto-generated method stub
		String url;
		url = "http://pklota-pklota.rhcloud.com/services/VideoPostServiceImpl/getAllVideoThumbPost";
		/*
		 * url = baseUrl + "stores?latitude=" + lat_current + "&longitude=" +
		 * lng_current+"&dpAkamaiOverride=1";
		 */
		// TODO Auto-generated method stub
		return url;
	}

	public static String getIndividualImageUrl(String id) {
		// TODO Auto-generated method stub
		String url;
		url = "http://pklota-pklota.rhcloud.com/services/ImagePostServiceImpl/getImagePostById/"+id;
		/*
		 * url = baseUrl + "stores?latitude=" + lat_current + "&longitude=" +
		 * lng_current+"&dpAkamaiOverride=1";
		 */
		// TODO Auto-generated method stub
		return url;
	}
}
