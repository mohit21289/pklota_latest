package com.pklota.android.activity;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;









import com.pklota.android.R;
import com.pklota.android.Fragment.PK_AudioFragment;
import com.pklota.android.Fragment.PK_ImageFragment;
import com.pklota.android.Fragment.PK_TextFragment;
import com.pklota.android.Fragment.PK_VideoFragment;
import com.pklota.android.Util.CustomDrawerAdapter;
import com.pklota.android.Util.DrawerItem;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


public class PKLota_MainActivity extends FragmentActivity {

	public static DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;

	private CharSequence mDrawerTitle;
	private CharSequence mTitle;
	CustomDrawerAdapter adapter;

	public static List<DrawerItem> dataList;

	private static final String IS_FIRST_VISIT = "isFirstVisit";

	private static final String PREF_FILE = "mybjs_pref";
	public static LinearLayout copyrigths;
	TextView txt_copyrigths;
	SharedPreferences prefs;
	String txt;
	String maddess;
	String mStatus;
	//ArrayList<ShoppingCartCount> m_cShoppingCartCount = new ArrayList<ShoppingCartCount>();
	//ArrayList<ValidateShoppingCartResult> m_cValidateShoppingCartResult = new ArrayList<ValidateShoppingCartResult>();
	private int m_cQuantityCount;
	public String m_cCustomerID;
	private String m_cSiteID;
	private String m_cSCItemCount;
	// private String
	// m_cValidateShoppingCartUrl="http://services.bjsrestaurants.com/BJS/OOS.svc/ValidateShoppingCart";
	private String m_cGetShoppingCartListWValidation = "http://services.bjsrestaurants.com/BJS/OOS.svc/GetShoppingCartListWValidation/";
	//List<SiteEntity> siteEntity = new ArrayList<SiteEntity>();
	ImageView imageview;
	TextView textViewCount;
	ProgressBar m_cCartProgressBar;
	String fragmentname;
	//ShoppingCart m_cShoppingCart;
	public static boolean m_cHomeMenuPressed = false;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bjs_mainactivity);
		// Initializing

		System.out.println("-------" + txt);

		dataList = new ArrayList<DrawerItem>();
		mTitle = mDrawerTitle = getTitle();
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.list_slidermenu);
		copyrigths = (LinearLayout) findViewById(R.id.copyrigths);
		copyrigths.setVisibility(View.VISIBLE);
		// mDrawerLayout.setDrawerShadow(R.drawable.ic_drawer,
		// GravityCompat.START);

		this.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		// Add Drawer Item to dataList

		dataList.add(new DrawerItem("Text", R.drawable.text));
		dataList.add(new DrawerItem("Image", R.drawable.text));
		dataList.add(new DrawerItem("Audio", R.drawable.text));
		dataList.add(new DrawerItem("Video ", R.drawable.text));
		

		adapter = new CustomDrawerAdapter(this, R.layout.drawer_list_item,
				dataList);

		mDrawerList.setAdapter(adapter);

		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

		getActionBar().setDisplayHomeAsUpEnabled(true);
		 getActionBar().setHomeButtonEnabled(true);
		getActionBar().setDisplayShowTitleEnabled(true);
		getActionBar().setDisplayUseLogoEnabled(true);
	//	getActionBar().setIcon(R.color.transprant);

		/*m_cShoppingCart = new ShoppingCart(Bjs_MainActivity.this);
		m_cShoppingCart.open();*/

		prefs = PreferenceManager
				.getDefaultSharedPreferences(PKLota_MainActivity.this);

		// prefs.getString("address",BJs_ListAddressFragment.mSiteaddress);
		System.out.println("Address::." + maddess);
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.slider, R.string.action_settings,
				R.string.action_settings) {
			public void onDrawerClosed(View view) {
				// getActionBar().setTitle(mTitle);
				// invalidateOptionsMenu(); // creates call to
				// onPrepareOptionsMenu()
			}

			public void onDrawerOpened(View drawerView) {
				// getActionBar().setTitle(mDrawerTitle);
				// Toast.makeText(getApplicationContext(),"Drawer Opened",
				// Toast.LENGTH_SHORT).show();

				PKLota_MainActivity.this
						.getWindow()
						.setSoftInputMode(
								WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

				/*mStatus = SharedPreferenceData.getCustomerDetails(
						getApplicationContext()).getString(
						SharedPreferenceData.STATUS, null);
				String cust = SharedPreferenceData.getCustomerDetails(
						getApplicationContext()).getString(
						SharedPreferenceData.CUSTOMERID, null);
				System.out.println("---- Staus----" + mStatus);
				System.out.println("-- cust--" + cust);*/

				/*
				 * prefs = PreferenceManager
				 * .getDefaultSharedPreferences(Bjs_MainActivity.this); txt =
				 * prefs.getString("status", LoginFragment.mStatus);
				 */

				if (mStatus != null) {
					/*
					 * if (mStatus.equalsIgnoreCase("1")) { //
					 * dataList.remove(2); // dataList.add(2,new
					 * DrawerItem("Logout", 0) );
					 * 
					 * String logout = dataList.get(9).getItemName(); logout =
					 * dataList.get(9).getItemName() .replace("Login/Create",
					 * "Logout"); dataList.get(9).setItemName(logout);
					 * adapter.notifyDataSetChanged(); } else if
					 * (mStatus.equalsIgnoreCase("3")) { String login =
					 * dataList.get(9).getItemName(); login =
					 * dataList.get(9).getItemName() .replace("Logout",
					 * "Login/Create");
					 * 
					 * //SharedPreferences.Editor.clear();
					 * dataList.get(9).setItemName(login);
					 * adapter.notifyDataSetChanged(); }
					 */
				}

				// invalidateOptionsMenu(); // creates call to
				// onPrepareOptionsMenu()
			}
		};
		/*if (savedInstanceState == null) {
			displayView(0);

		}*/

		mDrawerLayout.setDrawerListener(mDrawerToggle);

	}

	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// ShoppingCart shoppingCart = new ShoppingCart(this);
		// shoppingCart.open();
		// int count = shoppingCart.getProductNameCount();//getting the count
		// from db.

		getMenuInflater().inflate(R.menu.splash_screen, menu);
		RelativeLayout badgeLayout = (RelativeLayout) menu.findItem(
				R.id.shoppingcart).getActionView();
		imageview = (ImageView) badgeLayout.findViewById(R.id.cart);
		textViewCount = (TextView) badgeLayout.findViewById(R.id.actionbar_notifcation_textview);
		m_cCartProgressBar = (ProgressBar) badgeLayout.findViewById(R.id.cart_progressbar);
		// new GetShoppingCartListCount().execute();
		m_cSCItemCount = SharedPreferenceData.getSCItemCount(Bjs_MainActivity.this).getString(SharedPreferenceData.SCITEMCOUNT, null);
		m_cSiteID = SharedPreferenceData.getsaveSiteDetails(Bjs_MainActivity.this).getString(SharedPreferenceData.SITEID,null);

		if (copyrigths.getVisibility() == View.INVISIBLE || copyrigths.getVisibility() == View.GONE) {
			if (m_cSCItemCount != null && !m_cSCItemCount.equalsIgnoreCase("0") && m_cSiteID == null) {
				System.out.println("itemcount>>" + m_cSCItemCount+ "\n m_cSiteID>>" + m_cSiteID);
				imageview.setVisibility(View.VISIBLE);
				textViewCount.setVisibility(View.VISIBLE);
				textViewCount.setText(m_cSCItemCount);
				m_cCartProgressBar.setVisibility(View.GONE);
			} else {
				System.out.println("itemcount else>>" + m_cSCItemCount+ "\n m_cSiteID>>" + m_cSiteID);
			}
			// textViewCount.setText(String.valueOf(m_cQuantityCount));
			textViewCount.setOnClickListener(this);
			imageview.setOnClickListener(this);
		} else {
			// login screen disable it
			imageview.setVisibility(View.GONE);
			textViewCount.setVisibility(View.GONE);
			m_cCartProgressBar.setVisibility(View.GONE);
		}
		// } else {
		// no items in the cart disable it.
		// imageview.setVisibility(View.GONE);
		// textViewCount.setVisibility(View.GONE);
		// }
		return true;
	}
*/
	/**
	 * Diplaying fragment view for selected nav drawer list item
	 * */
	public void displayView(int position) {
		// update the main content by replacing fragments
		Fragment fragment = null;
		/*maddess = SharedPreferenceData.getsaveSiteDetails(this).getString(
				SharedPreferenceData.SITEADDRESS, null);*/
		switch (position) {
		case 0:

			fragment = new PK_TextFragment();
			fragmentname = "text";
			m_cHomeMenuPressed = true;
			copyrigths.setVisibility(View.INVISIBLE);
			mDrawerLayout
					.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
			mDrawerLayout.closeDrawers();
			break;
		case 1:
			fragment = new PK_ImageFragment();
			fragmentname = "image";
			//m_cHomeMenuPressed = true;
			copyrigths.setVisibility(View.INVISIBLE);
			mDrawerLayout
					.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
			mDrawerLayout.closeDrawers();

			break;
		case 2:
			fragment = new PK_AudioFragment();
			fragmentname = "audio";
			//m_cHomeMenuPressed = true;
			copyrigths.setVisibility(View.INVISIBLE);
			mDrawerLayout
					.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
			mDrawerLayout.closeDrawers();
			break;
		case 3:
			fragment = new PK_VideoFragment();
			fragmentname = "video";
			//m_cHomeMenuPressed = true;
			copyrigths.setVisibility(View.INVISIBLE);
			mDrawerLayout
					.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
			mDrawerLayout.closeDrawers();

			break;
	

		default:
			break;
		}

		if (fragment != null) {
			FragmentManager fragmentManager = getSupportFragmentManager();
			fragmentManager.beginTransaction()
					.replace(R.id.frame_container, fragment, fragmentname)
					.commit();

			// update selected item and title, then close the drawer
			mDrawerList.setItemChecked(position, true);
			setTitle(dataList.get(position).getItemName());
			// mDrawerLayout.closeDrawer(mDrawerLayout);
			// mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
		}
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggles
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// The action bar home/up action should open or close the drawer.
		// ActionBarDrawerToggle will take care of this.
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		// Handle action bar actions click
		switch (item.getItemId()) {
		/*
		 * case R.id.shoppingcart:
		 * 
		 * Fragment fragment = new BJs_ShoppingCartFragment(); if (fragment !=
		 * null) { FragmentManager fragmentManager = getFragmentManager();
		 * fragmentManager.beginTransaction() .add(R.id.frame_container,
		 * fragment) .addToBackStack(null).commit(); }
		 * 
		 * return true;
		 */
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private class DrawerItemClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {

			displayView(position);

		}
	}

	
	/*@Override
	public void onBackPressed() {
		try {
			m_cHomeMenuPressed = true;
			Fragment fragment = new PK_TextFragment();
			FragmentTransaction ft = getSupportFragmentManager()
					.beginTransaction();

			if ((getSupportFragmentManager().findFragmentByTag("text") != null)
					&& getSupportFragmentManager().findFragmentByTag(
							"text").isVisible()) {

				ft.remove(getSupportFragmentManager().findFragmentByTag(
						"text"));

				ft.replace(R.id.frame_container, fragment, "home")
						.addToBackStack(null).commit();
			} else if ((getSupportFragmentManager().findFragmentByTag(
					"imgae") != null)
					&& getSupportFragmentManager().findFragmentByTag(
							"image").isVisible()) {
				ft.remove(getSupportFragmentManager().findFragmentByTag(
						"image"));
				Fragment fragmentShoppingCart = new BJs_ShoppingCartFragment();
				ft.replace(R.id.frame_container, fragmentShoppingCart,
						"shoppingcart").addToBackStack(null).commit();

			} else if ((getSupportFragmentManager().findFragmentByTag(
					"confirmorder") != null)
					&& getSupportFragmentManager().findFragmentByTag(
							"confirmorder").isVisible()) {
				ft.remove(getSupportFragmentManager().findFragmentByTag(
						"confirmorder"));
				ft.replace(R.id.frame_container, fragment, "home")
						.addToBackStack(null).commit();
			} else if ((getSupportFragmentManager().findFragmentByTag(
					"getdirections") != null)
					&& getSupportFragmentManager().findFragmentByTag(
							"getdirections").isVisible()) {
				ft.remove(getSupportFragmentManager().findFragmentByTag(
						"getdirections"));
				ft.replace(R.id.frame_container, fragment, "home")
						.addToBackStack(null).commit();
			} else if ((getSupportFragmentManager().findFragmentByTag("home") != null)
					&& getSupportFragmentManager().findFragmentByTag("home")
							.isVisible()) {

				exitDialog();

			} else if ((getSupportFragmentManager().findFragmentByTag(
					"loginfragment") != null)
					&& getSupportFragmentManager().findFragmentByTag(
							"loginfragment").isVisible()) {

				exitDialog();
			} else if (((getSupportFragmentManager().findFragmentByTag(
					"settingsfragment") != null) && getSupportFragmentManager()
					.findFragmentByTag("settingsfragment").isVisible())
					|| ((getSupportFragmentManager().findFragmentByTag(
							"loyalityfragment") != null) && getSupportFragmentManager()
							.findFragmentByTag("loyalityfragment").isVisible())
					|| ((getSupportFragmentManager().findFragmentByTag(
							"paymentfragment") != null) && getSupportFragmentManager()
							.findFragmentByTag("paymentfragment").isVisible())
					|| ((getSupportFragmentManager().findFragmentByTag(
							"locatorfragment") != null) && getSupportFragmentManager()
							.findFragmentByTag("locatorfragment").isVisible())
					|| ((getSupportFragmentManager().findFragmentByTag(
							"orderhistoryfragment") != null) && getSupportFragmentManager()
							.findFragmentByTag("orderhistoryfragment")
							.isVisible())
					|| ((getSupportFragmentManager().findFragmentByTag(
							"menuitemsfragment") != null) && getSupportFragmentManager()
							.findFragmentByTag("menuitemsfragment").isVisible())
					|| ((getSupportFragmentManager().findFragmentByTag(
							"selectmodefragment") != null) && getSupportFragmentManager()
							.findFragmentByTag("selectmodefragment")
							.isVisible())
					|| ((getSupportFragmentManager().findFragmentByTag(
							"waitinglistfragment") != null) && getSupportFragmentManager()
							.findFragmentByTag("waitinglistfragment")
							.isVisible())
					|| ((getSupportFragmentManager().findFragmentByTag(
							"whatsnewfragment") != null) && getSupportFragmentManager()
							.findFragmentByTag("whatsnewfragment").isVisible())
					|| ((getSupportFragmentManager().findFragmentByTag(
							"listofrestaurant") != null) && getSupportFragmentManager()
							.findFragmentByTag("listofrestaurant").isVisible())
					|| ((getSupportFragmentManager().findFragmentByTag(
							"maprestaurant") != null) && getSupportFragmentManager()
							.findFragmentByTag("maprestaurant").isVisible())

			) {
				ft.replace(R.id.frame_container, fragment, "home")
						.addToBackStack(null).commit();
			} else if ((getSupportFragmentManager().findFragmentByTag(
					"forgotpwdfragment") != null)
					&& getSupportFragmentManager().findFragmentByTag(
							"forgotpwdfragment").isVisible()) {
				Bjs_LoginScreen loginfragment = new Bjs_LoginScreen();
				ft.replace(R.id.frame_container, loginfragment, "loginfragment")
						.commit();
			} else {
				super.onBackPressed();
			}

		} catch (Exception e) {
			System.exit(0);

		}

	}*/

	private void exitDialog() {
		AlertDialog.Builder appexit = new AlertDialog.Builder(
				PKLota_MainActivity.this);
		appexit.setMessage("Are you sure you want to exit?");
		appexit.create();
		appexit.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				System.exit(0);
			}
		});
		appexit.setNegativeButton("No", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub

			}
		});
		appexit.show();

	}

	
}
