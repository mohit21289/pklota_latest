package com.pklota.android.adopter;

import java.util.ArrayList;
import java.util.Random;

import com.pklota.android.R;
import com.pklota.android.Pojo.AudioPostTO;
import com.pklota.android.Pojo.VideoThumbPostTO;
import com.pklota.android.grid.util.DynamicHeightTextView;

import android.content.Context;
import android.database.DataSetObserver;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

public class CustomAudioAdapter extends BaseAdapter {
	Context activity;
	ArrayList<AudioPostTO> videoThumbPostTO;
	Random mRandom;
	private static final SparseArray<Double> sPositionHeightRatios = new SparseArray<Double>();
	public CustomAudioAdapter(Context activity,
			ArrayList<AudioPostTO> arrayList) {
		// TODO Auto-generated constructor stub
		this.activity = activity;
		this.videoThumbPostTO = arrayList;
		mRandom = new Random();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return videoThumbPostTO.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return videoThumbPostTO.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final int pos = position;
		LayoutInflater inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = inflater.inflate(R.layout.list_item_sample, null);

		final ImageView name = (ImageView) convertView
				.findViewById(R.id.txt_line1);

	/*	Picasso.with(activity)
				.load(videoThumbPostTO.get(pos).getAudioUrl())
				.into(name);
*/
		// name.setBackgroundResource();

		try {
			double positionHeight = getPositionRatio(position);
		//	name.setHeightRatio(positionHeight);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return convertView;
	}

	private double getPositionRatio(final int position) {
		double ratio = sPositionHeightRatios.get(position, 0.0);
		// if not yet done generate and stash the columns height
		// in our real world scenario this will be determined by
		// some match based on the known height and width of the image
		// and maybe a helpful way to get the column height!
		if (ratio == 0) {
			ratio = getRandomHeightRatio();
			sPositionHeightRatios.append(position, ratio);
			// Log.d(TAG, "getPositionRatio:" + position + " ratio:" + ratio);
		}
		return ratio;
	}

	private double getRandomHeightRatio() {
		return (mRandom.nextDouble() / 2.0) + 1.0; // height will be 1.0 - 1.5
													// the width
	}
}
