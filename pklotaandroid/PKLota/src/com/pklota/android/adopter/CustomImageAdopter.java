package com.pklota.android.adopter;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.pklota.android.R;
import com.pklota.android.Controller.Controller;
import com.pklota.android.Pojo.ImageThumbPostTO;
import com.pklota.android.Pojo.RequestTypes;
import com.pklota.android.Pojo.SignleImage;
import com.pklota.android.Util.TouchImageView;
import com.pklota.android.grid.util.DynamicHeightImageView;
import com.pklota.android.listener.PK_SingleImageListener;
import com.squareup.picasso.Picasso;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Toast;

public class CustomImageAdopter extends BaseAdapter implements
		PK_SingleImageListener {
	Context ctx;
	private final Random mRandom;
	ArrayList<ImageThumbPostTO> listarray;
	private static final SparseArray<Double> sPositionHeightRatios = new SparseArray<Double>();
	boolean bol;
	DisplayImageOptions options;
	ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
	PK_SingleImageListener listener;
	Uri uri;
	private static Bitmap loadedImage;
	boolean plusbtn = false;

	public CustomImageAdopter(Context ctx,
			ArrayList<ImageThumbPostTO> arrayList, boolean bol) {
		this.ctx = ctx;
		this.listarray = arrayList;
		this.bol = bol;
		mRandom = new Random();
		options = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.ic_stub)
				.showImageForEmptyUri(R.drawable.ic_empty)
				.showImageOnFail(R.drawable.ic_error).cacheInMemory(true)
				.cacheOnDisk(true).considerExifParams(true)
				.displayer(new RoundedBitmapDisplayer(20)).build();

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return listarray.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@SuppressLint("NewApi")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		boolean flag = false;
		final int pos = position;
		LayoutInflater inflater = (LayoutInflater) ctx
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = inflater.inflate(R.layout.list_item_sample, null);
		try {

			final DynamicHeightImageView imageView = (DynamicHeightImageView) convertView
					.findViewById(R.id.txt_line1);
			final ImageView downloadimg = (ImageView) convertView
					.findViewById(R.id.downloadimg);
			final String root = Environment.getExternalStorageDirectory()
					.toString();
			File myDir = new File(root + "/DCIM/PK");
			if (myDir != null && myDir.exists() && myDir.isDirectory()) {
				File[] listFile = myDir.listFiles();

				for (int i = 0; i < listFile.length; i++) {
					if (listFile[i].getName().equalsIgnoreCase(
							(listarray.get(pos).getIpid()) + ".jpg")) {
						downloadimg.setVisibility(View.GONE);
						uri = Uri.fromFile(listFile[i].getAbsoluteFile());
						imageView.setImageURI(uri);
						flag = true;
					}
				}
				if (!flag) {
					ImageLoader.getInstance().displayImage(
							listarray.get(pos).getImageThumbUrl(), imageView,
							options, new ImageLoadingListener() {
								@Override
								public void onLoadingStarted(String imageUri,
										View view) {
									//view.animate();
									//view.animate().translationX(400).withLayer();
								}

								@Override
								public void onLoadingFailed(String imageUri,
										View view, FailReason failReason) {

								}

								@Override
								public void onLoadingComplete(String imageUri,
										View view, Bitmap loadedImage) {

									final List<String> displayedImages = Collections
											.synchronizedList(new LinkedList<String>());

									if (loadedImage != null) {
										ImageView imageView = (ImageView) view;
										boolean firstDisplay = !displayedImages
												.contains(imageUri);
										if (firstDisplay) {
											FadeInBitmapDisplayer.animate(
													imageView, 500);
											displayedImages.add(imageUri);
										}
									}

								}

								@Override
								public void onLoadingCancelled(String imageUri,
										View view) {

								}
							}, new ImageLoadingProgressListener() {
								@Override
								public void onProgressUpdate(String imageUri,
										View view, int current, int total) {
								}
							});
					flag = false;
				}

			} else {

				ImageLoader.getInstance().displayImage(
						listarray.get(pos).getImageThumbUrl(), imageView,
						options, new ImageLoadingListener() {
							@Override
							public void onLoadingStarted(String imageUri,
									View view) {
							}

							@Override
							public void onLoadingFailed(String imageUri,
									View view, FailReason failReason) {

							}

							@Override
							public void onLoadingComplete(String imageUri,
									View view, Bitmap loadedImage) {

								final List<String> displayedImages = Collections
										.synchronizedList(new LinkedList<String>());

								if (loadedImage != null) {
									ImageView imageView = (ImageView) view;
									boolean firstDisplay = !displayedImages
											.contains(imageUri);
									if (firstDisplay) {
										FadeInBitmapDisplayer.animate(
												imageView, 500);
										displayedImages.add(imageUri);
									}
								}

							}

							@Override
							public void onLoadingCancelled(String imageUri,
									View view) {

							}
						}, new ImageLoadingProgressListener() {
							@Override
							public void onProgressUpdate(String imageUri,
									View view, int current, int total) {

							}
						});
			}
			convertView.setOnClickListener(new View.OnClickListener() {
				private Controller mController;

				@Override
				public void onClick(View v) {
					if (downloadimg.getVisibility() == View.VISIBLE) {
						mController = Controller.getInstance(ctx);
						mController
								.setListenerForImage(CustomImageAdopter.this);
						mController.getIndividualImageOperation(listarray.get(
								pos).getIpid());
					} else {
						File myDir = new File(root + "/DCIM/PK");
						if (myDir != null && myDir.exists()
								&& myDir.isDirectory()) {
							File[] listFile = myDir.listFiles();

							for (int i = 0; i < listFile.length; i++) {
								if (listFile[i].getName()
										.equalsIgnoreCase(
												(listarray.get(pos).getIpid())
														+ ".jpg")) {
									uri = Uri.fromFile(listFile[i]
											.getAbsoluteFile());
									showCustomDialogForImage(uri.toString(),
											listarray.get(pos).getIpid(),
											plusbtn);
								}
							}

						}
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return convertView;
	}

	private void storeInLocaleFile(Bitmap loadedImage, String id, Context ctx2) {
		String root = Environment.getExternalStorageDirectory().toString();
		File myDir = new File(root + "/DCIM/PK");
		myDir.mkdirs();
		Random generator = new Random();
		int n = 10000;
		n = generator.nextInt(n);
		String fname = id + ".jpg";
		File file = new File(myDir, fname);
		if (file.exists())
			file.delete();
		try {
			FileOutputStream out = new FileOutputStream(file);
			loadedImage.compress(Bitmap.CompressFormat.JPEG, 100, out);
			Toast.makeText(ctx2, "Saved Successfully", Toast.LENGTH_SHORT)
					.show();
			out.flush();
			out.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		notifyDataSetChanged();
	}

	@SuppressLint("NewApi")
	private void showCustomDialogForImage(final String string, final String id,
			boolean plusbutton) {
		final Dialog dialog = new Dialog(ctx);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_image_dialog);
		TouchImageView image = (TouchImageView) dialog.findViewById(R.id.img);
		ImageView imagesave = (ImageView) dialog.findViewById(R.id.img_save);
		if (!plusbutton) {
			imagesave.setVisibility(View.GONE);
		}
		Picasso.with(ctx).load(string).placeholder(R.drawable.loding).
				into(image);
		
		//Picasso.with(ctx).load(string).placeholder(new Drawable) .into(image);
		
		ImageView share_img = (ImageView) dialog.findViewById(R.id.img_share);
		share_img.setOnClickListener(new View.OnClickListener() {

			private String image_path;
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent share = new Intent(android.content.Intent.ACTION_SEND);
				share.setType("image/*");
				share.putExtra(Intent.EXTRA_STREAM,
						Uri.parse("file://" + image_path)); // Add
				ctx.startActivity(Intent.createChooser(share,
						"Share image using"));

			}
		});
		imagesave.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					storeInLocaleFile(loadedImage, id, ctx);
					dialog.dismiss();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});
		dialog.show();
	}

	public void showDownloadDilog() {
		ProgressDialog progressDialog = new ProgressDialog(ctx);
		progressDialog.setContentView(R.layout.progress_dialog);
		progressDialog.setCancelable(false);
		progressDialog.show();
	}

	private static class AnimateFirstDisplayListener extends
			SimpleImageLoadingListener {

		static final List<String> displayedImages = Collections
				.synchronizedList(new LinkedList<String>());

		@Override
		public void onLoadingComplete(String imageUri, View view,
				Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				boolean firstDisplay = !displayedImages.contains(imageUri);
				if (firstDisplay) {
					FadeInBitmapDisplayer.animate(imageView, 500);
					displayedImages.add(imageUri);
					CustomImageAdopter.loadedImage = loadedImage;
				}

			}
		}

	}

	@Override
	public void getSingleImageDataResponse(SignleImage signleImage,
			RequestTypes reqType) {
		showCustomDialogForImage(signleImage.getImageUrl(),
				signleImage.getIpid(), true);
	}

	private double getRandomHeightRatio() {
		return (mRandom.nextDouble() / 2) + 10.0; 
	}
}
