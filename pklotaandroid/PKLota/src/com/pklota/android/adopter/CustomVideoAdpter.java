package com.pklota.android.adopter;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;




import com.pklota.android.R;

import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class CustomVideoAdpter extends BaseAdapter {
	Context ctx;
	ArrayList<HashMap<String, String>> videolist;

	public CustomVideoAdpter(Context ctx,
			ArrayList<HashMap<String, String>> videolist) {
		this.ctx = ctx;
		this.videolist = videolist;
		// TODO Auto-generated constructor stub
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return videolist.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return videolist.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final int pos = position;
		LayoutInflater inflater = (LayoutInflater) ctx
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = inflater.inflate(R.layout.list_item_row, null);
		try {
			TextView name = (TextView) convertView.findViewById(R.id.FilePath);
			ImageView thumbnailimg = (ImageView) convertView
					.findViewById(R.id.Thumbnail);
			HashMap<String, String> hm = videolist.get(position);
			name.setText(hm.get("imagepath"));
			String path = hm.get("thumbnail");

			Bitmap bm = StringToBitMap(path);
			thumbnailimg.setImageBitmap(bm);
			// Uri uri = Uri.fromFile(new File(path));
			/*
			 * Picasso.with(ctx) .load(uri) .error(R.drawable.share)
			 * .into(thumbnailimg, new EmptyCallback() {
			 * 
			 * @Override public void onSuccess() {
			 * progressBar.setVisibility(View.GONE); }
			 * 
			 * @Override public void onError() {
			 * progressBar.setVisibility(View.GONE); } });
			 */

			/*
			 * Picasso.with(ctx) .load(uri) .into(thumbnailimg);
			 */
			convertView.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					File file = new File(videolist.get(pos).get("imagepath"));
					Intent intent = new Intent(Intent.ACTION_VIEW);
					intent.setDataAndType(Uri.fromFile(file), "video/*");
					ctx.startActivity(intent);
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
		return convertView;
	}

	public Bitmap StringToBitMap(String encodedString) {
		try {
			byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
			Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0,
					encodeByte.length);
			return bitmap;
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}
}
