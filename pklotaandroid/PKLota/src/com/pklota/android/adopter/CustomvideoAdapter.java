package com.pklota.android.adopter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.pklota.android.R;
import com.pklota.android.Pojo.VideoThumbPostTO;
import com.pklota.android.grid.util.DynamicHeightTextView;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.support.v4.app.FragmentActivity;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

public class CustomvideoAdapter extends BaseAdapter {
	Context activity;
	ArrayList<VideoThumbPostTO> videoThumbPostTO;
	Random mRandom;
	private static final SparseArray<Double> sPositionHeightRatios = new SparseArray<Double>();
	DisplayImageOptions options;
	
	public CustomvideoAdapter(Context activity,
			ArrayList<VideoThumbPostTO> videoThumbPostTO) {
		// TODO Auto-generated constructor stub
		this.activity = activity;
		this.videoThumbPostTO = videoThumbPostTO;
		mRandom = new Random();
		options = new DisplayImageOptions.Builder()
		.showImageOnLoading(R.drawable.ic_stub)
		.showImageForEmptyUri(R.drawable.ic_empty)
		.showImageOnFail(R.drawable.ic_error).cacheInMemory(true)
		.cacheOnDisk(true).considerExifParams(true)
		.displayer(new RoundedBitmapDisplayer(20)).build();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return videoThumbPostTO.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return videoThumbPostTO.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final int pos = position;
		LayoutInflater inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = inflater.inflate(R.layout.list_item_sample, null);

		final ImageView imageView = (ImageView) convertView
				.findViewById(R.id.txt_line1);
		
		
		ImageLoader.getInstance().displayImage(
				videoThumbPostTO.get(pos).getVideoThumbUrl(), imageView,
				options, new ImageLoadingListener() {
					@Override
					public void onLoadingStarted(String imageUri,
							View view) {
					}

					@Override
					public void onLoadingFailed(String imageUri,
							View view, FailReason failReason) {

					}

					@Override
					public void onLoadingComplete(String imageUri,
							View view, Bitmap loadedImage) {

						final List<String> displayedImages = Collections
								.synchronizedList(new LinkedList<String>());

						if (loadedImage != null) {
							ImageView imageView = (ImageView) view;
							boolean firstDisplay = !displayedImages
									.contains(imageUri);
							if (firstDisplay) {
								FadeInBitmapDisplayer.animate(
										imageView, 500);
								displayedImages.add(imageUri);
							}
						}

					}

					@Override
					public void onLoadingCancelled(String imageUri,
							View view) {

					}
				}, new ImageLoadingProgressListener() {
					@Override
					public void onProgressUpdate(String imageUri,
							View view, int current, int total) {

					}
				});

		/*Picasso.with(activity)
				.load(videoThumbPostTO.get(pos).getVideoThumbnailUrl())
				.into(name);*/

		// name.setBackgroundResource();

		try {
			double positionHeight = getPositionRatio(position);
			//name.setHeightRatio(positionHeight);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return convertView;
	}

	private double getPositionRatio(final int position) {
		double ratio = sPositionHeightRatios.get(position, 0.0);
		// if not yet done generate and stash the columns height
		// in our real world scenario this will be determined by
		// some match based on the known height and width of the image
		// and maybe a helpful way to get the column height!
		if (ratio == 0) {
			ratio = getRandomHeightRatio();
			sPositionHeightRatios.append(position, ratio);
			// Log.d(TAG, "getPositionRatio:" + position + " ratio:" + ratio);
		}
		return ratio;
	}

	private double getRandomHeightRatio() {
		return (mRandom.nextDouble() / 2.0) + 1.0; // height will be 1.0 - 1.5
													// the width
	}
}
