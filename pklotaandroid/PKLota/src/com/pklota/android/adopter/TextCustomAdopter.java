package com.pklota.android.adopter;

import java.util.ArrayList;

import com.pklota.android.R;
import com.pklota.android.Pojo.TextPojo;
import com.pklota.android.Pojo.TextPostTO;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

public class TextCustomAdopter extends BaseAdapter {
	Context ctx;
	ArrayList<TextPostTO> arrayList;

	public TextCustomAdopter(Context ctx, ArrayList<TextPostTO> arrayList) {
		// TODO Auto-generated constructor stub
		this.ctx = ctx;
		this.arrayList = arrayList;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrayList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final int pos = position;
		LayoutInflater inflater = (LayoutInflater) ctx
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = inflater.inflate(R.layout.text_customlist, null);

		TextView post_content_tv = (TextView) convertView
				.findViewById(R.id.postContent);
		TextView postDate_tv = (TextView) convertView
				.findViewById(R.id.postDate);
		try {

			post_content_tv.setText(arrayList.get(pos).getPostContent());
			postDate_tv.setText(arrayList.get(pos).getPostDate());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return convertView;
	}

}
