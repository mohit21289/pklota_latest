package com.pklota.android.listener;

import com.pklota.android.Pojo.Response;

public interface OnTaskCompleted {
	void onTaskCompleted(Response response);

}
