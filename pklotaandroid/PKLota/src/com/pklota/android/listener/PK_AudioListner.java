package com.pklota.android.listener;

import com.pklota.android.Pojo.AudioPojo;
import com.pklota.android.Pojo.RequestTypes;
import com.pklota.android.grid.util.DynamicHeightTextView;

import android.app.Dialog;
import android.widget.ImageView;

public interface PK_AudioListner {
	public void getAudioDataResponse(String result);

	public void getAudioDataResponse(AudioPojo parseAudioData,
			RequestTypes reqType);

	public void onFailure(String data, RequestTypes reqType);
}
