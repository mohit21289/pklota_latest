package com.pklota.android.listener;

import com.pklota.android.Pojo.ImagePojo;
import com.pklota.android.Pojo.RequestTypes;
import com.pklota.android.grid.util.DynamicHeightTextView;

import android.app.Dialog;
import android.widget.ImageView;

public interface PK_ImageListner {
	public void getImageDataResponse(ImagePojo parseImageData,
			RequestTypes reqType);
	public void onFailure(String data, RequestTypes reqType);
	
}
