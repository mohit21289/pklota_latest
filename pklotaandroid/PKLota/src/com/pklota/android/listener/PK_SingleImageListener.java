package com.pklota.android.listener;

import com.pklota.android.Pojo.ImagePojo;
import com.pklota.android.Pojo.RequestTypes;
import com.pklota.android.Pojo.SignleImage;

public interface PK_SingleImageListener {
	public void getSingleImageDataResponse(SignleImage signleImage,
			RequestTypes reqType);
}
