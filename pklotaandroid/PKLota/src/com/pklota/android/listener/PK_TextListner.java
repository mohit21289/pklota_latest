package com.pklota.android.listener;

import com.pklota.android.Pojo.RequestTypes;
import com.pklota.android.Pojo.TextPojo;
import com.pklota.android.grid.util.DynamicHeightTextView;

import android.app.Dialog;
import android.widget.ImageView;

public interface PK_TextListner {
	public void getTextDataResponse(TextPojo textPojo, RequestTypes reqType);

	public void onFailure(String data, RequestTypes reqType);
}
