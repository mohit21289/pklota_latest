package com.pklota.android.listener;

import com.pklota.android.Pojo.RequestTypes;
import com.pklota.android.Pojo.VideoPojo;
import com.pklota.android.grid.util.DynamicHeightTextView;

import android.app.Dialog;
import android.widget.ImageView;

public interface PK_VideoListner {

	public void onFailure(String data, RequestTypes reqType);


	public void getVideoDataResponse(VideoPojo parseVideoData, RequestTypes reqType);


	public void getSingleVideoDataResponse(VideoPojo parseSingleVideoData,
			RequestTypes reqType);
}
